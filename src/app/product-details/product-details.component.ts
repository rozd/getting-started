import {Component, OnInit, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Product, products } from '../products';
import { CartService } from '../cart.service';


function Reactivity<T extends { new (...args: any[]): {} }>(constructor: T) {
  // console.log(`>>> Reactivity: ${constructor}`);
  for (const constructorKey in constructor) {
    console.log(`>>> constructor[${constructorKey}]=${constructor[constructorKey]}`);
  }
  return class extends constructor {
    reportingURL = "http://www...";
  };
}

export function Reactive2(targetClass: any, functionName: string, descriptor: PropertyDescriptor) {

}

// export function Reactive() {
//   return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
//     descriptor.enumerable = value;
//   };
// }

function enumerable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.enumerable = value;
  };
}

export declare const Input2: InputDecorator2;

export declare interface InputDecorator2 {
  // (bindingPropertyName?: string): any;
  new (bindingPropertyName?: string): any;
}

// @ts-ignore
// function logged(value: any, { kind, name }) {
//   if (kind === "method") {
//     return function (...args: any[]) {
//       console.log(`starting ${name} with arguments ${args.join(", ")}`);
//       // @ts-ignore
//       const ret = value.call(this, ...args);
//       console.log(`ending ${name}`);
//       return ret;
//     };
//   }
// }

export function Confirmable (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
  // Again, cache the original method for later use
  const originalMethod = descriptor.value;
  // the configuration object for sweetalert
  // DestroyRef
  // ComponentRef

  // we write a new implementation for the method
  descriptor.value = async function (...args: any[]) {
    window.alert('Hi');
    // fire sweetalert with the config object
    // if (res.isConfirmed){ // if user clicked yes,
    //   // we run the original method with the original arguments
    //   const result = originalMethod.apply(this, args);
    //
    //   // and return the result
    //   return result;
    // }
  };
  return descriptor;
}

export function ReactiveInput(target: Object, propertyKey: string) {
  Object.defineProperty(target, 'ngOnChanges', {
    value: (changes: SimpleChanges) => {
      console.log(`defined ngOnChanges: ${changes['product']}`);
    },
    writable: false,
    enumerable: true,
    configurable: false,
  })
}


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product: Product | undefined;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService
  ) { }

  ngOnInit() {
    // First get the product id from the current route.
    const routeParams = this.route.snapshot.paramMap;
    const productIdFromRoute = Number(routeParams.get('productId'));

    // Find the product that correspond with the id provided in route.
    this.product = products.find(product => product.id === productIdFromRoute);
  }

  addToCart(product: Product) {
    this.cartService.addToCart(product);
    window.alert('Your product has been added to the cart!');
  }
}
