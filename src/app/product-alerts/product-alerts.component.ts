import {
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges
} from '@angular/core';
import {Product} from '../products';
import {Observable, Subject} from "rxjs";

function Reactivity<T extends { new (...args: any[]): {} }>(constructor: T) {
  // console.log(`>>> Reactivity: ${constructor}`);
  for (const constructorKey in constructor) {
    console.log(`>>> constructor[${constructorKey}]=${constructor[constructorKey]}`);
  }
  return class extends constructor {
    reportingURL = "http://www...";
  };
}

function ReactiveProperty(target: Object, propertyKey: string) {

  Object.defineProperty(target, 'ngOnChanges', {
    value: (changes: SimpleChanges) => {

    },
    writable: false,
    enumerable: true,
    configurable: false,
  })
}

export function Confirmable (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
  // Again, cache the original method for later use
  const originalMethod = descriptor.value;
  // the configuration object for sweetalert
  // DestroyRef
  // ComponentRef

  // we write  a new implementation for the method
  descriptor.value = async function (...args: any[]) {
    window.alert('Hi');
    // fire sweetalert with the config object
    // if (res.isConfirmed){ // if user clicked yes,
    //   // we run the original method with the original arguments
    //   const result = originalMethod.apply(this, args);
    //
    //   // and return the result
    //   return result;
    // }
  };
  return descriptor;
}

export function ReactiveInput(target: Object, propertyKey: string) {
  // @ts-ignore
  console.log(`~ ${String((target as ProductAlertsComponent).test)}`);
  for (const targetKey in target) {
    console.log(`>>> target[${targetKey}]=${(target as any)[targetKey]}`);
  }
  // const subject = target[`${propertyKey}$`] as Subject<any>;
  Object.defineProperty(target, 'ngOnChanges', {
    value: (changes: SimpleChanges) => {
      console.log(`> ${Object.hasOwn(target, 'product$')}`);
      // @ts-ignore
      console.log(`> ${target['product$']}`);
      console.log(`defined ngOnChanges: ${changes['product']}`);
    },
    writable: false,
    enumerable: true,
    configurable: false,
  })
}

//---------

@Reactivity
@Component({
  selector: 'app-product-alerts',
  templateUrl: './product-alerts.component.html',
  styleUrls: ['./product-alerts.component.css']
})
@Reactivity
export class ProductAlertsComponent {
  @Input()
  @ReactiveInput
  product: Product | undefined;
  product$ = new Subject<Product>();

  // @ReactiveProperty
  product_!: Subject<Product>;


  @Input()
  // @ReactiveInput
  test: string | undefined;

  // @Reactive()
  // product$: Observable<Product> | undefined;

  @Output() notify = new EventEmitter();

  constructor() {
    console.log(`ProductAlertsComponent`);
  }

  // @enumerable(false)
  // greet() {
  //   return "Hello, " + this.greeting;
  // }

  @Confirmable
  onNotifyTapped() {
    this.notify.emit();
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   console.log(`changes: ${changes}`);
  // }
}
